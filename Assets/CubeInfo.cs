using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CubeType
{
    canWalk,
    cannotWalk
}

public class CubeInfo : MonoBehaviour
{
    public int x;
    public int y;

    public CubeType cubeType = CubeType.canWalk;

    public float pathConsume = 0;

    public GameObject parentObj = null;

    public bool alreadySet = false;

    public void SetPos(int x,int y)
    {
        this.x = x;
        this.y = y;
    }
}
