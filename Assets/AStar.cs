using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar : MonoBehaviour
{
    public static AStar _instance;
    public void Awake()
    {
        _instance = this;
    }

    public CubeInfo[,] objArr = new CubeInfo[10, 10];

    public List<CubeInfo> openLists = new List<CubeInfo>();
    public List<CubeInfo> closeLists = new List<CubeInfo>();

    public CubeInfo start;
    public CubeInfo end;

    public void Astar_Connect()
    {
         start = objArr[(int)TerrainGeneration._instance.startPos.x, (int)TerrainGeneration._instance.startPos.y];
         end = objArr[(int)TerrainGeneration._instance.endPos.x, (int)TerrainGeneration._instance.endPos.y];

        if(start.cubeType==CubeType.cannotWalk||end.cubeType==CubeType.cannotWalk)
        {
            Debug.Log("出生点或驻点为不可选取点，寻路失败");
            return;
        }

        start.alreadySet = true;
        openLists.Add(start);
        //int test = 0;
// !(start.x == end.x && start.y == end.y)
        while (true)
        {
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if ((start.x + i) >= 10 || (start.x + i) < 0 || (start.y + j) >= 10 || (start.y + j) < 0 || (i == 0 && j == 0)||start.cubeType==CubeType.cannotWalk)
                    {
                        //Debug.Log("outRange");
                        continue;
                    }
                    var temp = objArr[start.x + i, start.y + j];
                    if(temp.alreadySet)
                    {
                        continue;
                    }
                    temp.parentObj = start.gameObject;
                    temp.pathConsume = Vector2.Distance(new Vector2(start.x, start.y), new Vector2(temp.x, temp.y)) + Mathf.Abs(end.x - temp.x) + Mathf.Abs(end.y - temp.y);
                    temp.alreadySet = true;
                    closeLists.Add(temp);
                }
            }
            //进行一轮计算后，对关闭列表进行排序

            closeLists.Sort((CubeInfo a, CubeInfo b) =>
            {
                if (a.pathConsume > b.pathConsume)
                    return 1;
                else if (a.pathConsume == b.pathConsume)
                    return 1;
                else
                    return -1;
            });

            //Debug.Log(closeLists[0].name);

            start = closeLists[0];

            openLists.Add(closeLists[0]);

            if (start.x == end.x && start.y == end.y)
            {
                GameObject t = start.gameObject;
                Color color = RandomColor();
                while (t.GetComponent<CubeInfo>().parentObj != null)
                {
                    t.GetComponent<MeshRenderer>().material.color = color;
                    t = t.GetComponent<CubeInfo>().parentObj;
                    //Debug.Log(t.name);
                }
                return;
            }

            closeLists.Remove(start);
            if (closeLists.Count == 0)
            {
                Debug.Log("关闭列表为空，寻路失败");
                return;
            }
        }
        Debug.Log("寻路成功");
    }

    Color RandomColor()
    {
        //随机颜色的RGB值。即刻得到一个随机的颜色
        float r = Random.Range(0f, 1f);
        float g = Random.Range(0f, 1f);
        float b = Random.Range(0f, 1f);
        Color color = new Color(r, g, b);
        return color;
    }
}
